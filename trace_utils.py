# -*- coding: utf-8
from __future__ import print_function
import json
import os
import io
import datetime
from dateutil.tz import tzutc
import dateutil.parser
from hashlib import md5
from typing import Callable


class ProcessedEvent:
    def __init__(self, data, originalEvents=None):
        self.data = data
        if originalEvents is None:
            self.originalEvents = None
        elif isinstance(originalEvents, ProcessedEvent):
            self.originalEvents = originalEvents.originalEvents
        elif isinstance(originalEvents, list):
            events = []
            for src in originalEvents:
                if isinstance(src, ProcessedEvent):
                    if src.originalEvents is None:
                        continue
                    for event in src.originalEvents:
                        events.append(event)
                else:
                    events.append(src)
            self.originalEvents = events
        else:
            self.originalEvents = [originalEvents]

    def getOriginalEvents(self):
        return self.originalEvents

    def toJson(self):
        return json.dumps(
            {'data': self.data, 'originals': self.originalEvents})

    def getTimestamp(self):
        """
        try to get a timestamp for this event, return None otherwise.

        This method might be placed outside of this class, but as we are supposed to work only with timestamped data, it makes sense to keep it here.
        """
        if isinstance(self.data, dict) and 'timestamp' in self.data.keys():
            return self.data['timestamp']
        else:
            nb = 0
            sum = 0
            for orig in self.originalEvents:
                if isinstance(orig, dict)and 'timestamp' in orig.keys():
                    sum += orig['timestamp']
                    nb += 1
            if nb == 0:
                return None
            return sum / nb

    @staticmethod
    def fromJson(jsonString):
        dct = json.loads(jsonString)
        return ProcessedEvent(dct['data'], dct['originals'])


def defaultEventToAgilkia(event: ProcessedEvent) -> 'agilkia.Event':
    """
    Simple and stupid converter from an event from trace_utils to an agilkia event.
    Works only in few cases, but it can help for experiments.
    """
    import agilkia
    if isinstance(event.data, str):
        return agilkia.Event(event.data, {}, {}, {})
    print("cannot simply convert event to agilkia event : ", event.data)
    return agilkia.Event(event.data, {}, {}, {})


class BaseProcessedTraces:
    def asSimpleGenerator(self):
        raise NotImplementedError

    def asSimpleList(self):
        raise NotImplementedError

    def asAgilkiaTraceSet(self,
                          eventToAgilkia: Callable[[ProcessedEvent],
                                                   'agilkia.Event'] = defaultEventToAgilkia) -> 'agilkia.TraceSet':
        """
        convert traces into an agilkia traceSet, each event being translated with the method eventToAgilkia provided as a parameter
        """
        import agilkia
        traceSet = agilkia.TraceSet([])
        for trace in self:
            aTrace = agilkia.Trace([])
            for event in trace:
                aTrace.append(eventToAgilkia(event))
            traceSet.append(aTrace)
        return traceSet


class ProcessedTrace:
    def __iter__(self):
        raise NotImplementedError


class ProcessedTraces(BaseProcessedTraces):
    def __iter__(self):
        raise NotImplementedError

    def asSimpleGenerator(self):
        def simpleTrace(trace):
            for event in trace:
                yield event.data
        for trace in self:
            yield simpleTrace(trace)

    def asSimpleList(self):
        traces = []
        for trace in self:
            simpleTrace = []
            for event in trace:
                simpleTrace.append(event.data)
            traces.append(simpleTrace)
        return traces

    def asEventList(self):
        traces = []
        for trace in self:
            simpleTrace = []
            for event in trace:
                simpleTrace.append(event)
            traces.append(simpleTrace)
        return traces

    @staticmethod
    def fromBase(traces):
        if isinstance(traces, OrderedProcessedTraces):
            return OrderedToProcessedTraces(traces)
        assert isinstance(traces, ProcessedTraces)
        return traces


class RamProcessedTrace(ProcessedTrace):
    def __init__(self, source):
        self.generator = source.__iter__()
        self.events = []

    def load(self, nb=-1):
        try:
            while self.generator is not None and nb != 0:
                nb -= 1
                self.events.append(next(self.generator))
        except StopIteration:
            self.generator = None

    def isFullyLoaded(self):
        return self.generator is None

    def __len__(self):
        self.load()
        return len(self.events)

    def __iter__(self):
        class Iterator:
            def __init__(self, parent):
                self.parent = parent
                self.pos = 0

            def __next__(self):
                if self.pos == len(
                        self.parent.events)and not self.parent.isFullyLoaded():
                    self.parent.load(nb=1)
                if self.pos < len(self.parent.events):
                    event = self.parent.events[self.pos]
                    self.pos += 1
                    return event
                raise StopIteration
        return Iterator(self)


class OrderedProcessedTraces(BaseProcessedTraces):
    @staticmethod
    def endOfTrace():
        return None

    @staticmethod
    def isEndOfTrace(event):
        return event is None

    @staticmethod
    def fromBase(traces):
        if isinstance(traces, ProcessedTraces):
            return ProcessedTracesToOrdered(traces)
        assert isinstance(traces, OrderedProcessedTraces)
        return traces

    def __iter__(self):
        """the iterator will provide events or a special value indicating end of a Trace"""
        raise NotImplementedError


class OrderedToProcessedTraces(ProcessedTraces):
    def __init__(self, traces):
        assert isinstance(traces, OrderedProcessedTraces)
        self.traces = traces

    def __iter__(self):
        class Trace(ProcessedTrace):
            def __init__(self, parentIterator):
                self.iterator = TraceIterator(parentIterator)
                self.used = False

            def __iter__(self):
                if self.used:
                    raise LookupError(
                        "You can iterate only one time over this trace.")
                self.used = True
                return self.iterator

            def load(self, nb=-1):
                self.iterator.load(nb)

        class TraceIterator:
            def __init__(self, iterator):
                self.iterator = iterator
                self.buffer = []

            def load(self, nb):
                while self.iterator is not None and nb != 0:
                    event = next(self.iterator)
                    nb -= 1
                    if OrderedProcessedTraces.isEndOfTrace(event):
                        self.iterator = None
                    else:
                        self.buffer.append(event)

            def __next__(self):
                if len(self.buffer) == 0:
                    self.load(nb=1)
                    if len(self.buffer) == 0:
                        raise StopIteration
                event = self.buffer.pop(0)
                return event

        class TracesIterator:
            def __init__(self, parent):
                self.parent = parent
                self.traces = parent.traces.__iter__()
                self.currentTrace = None

            def __next__(self):
                if self.currentTrace is not None:
                    self.currentTrace.load()
                self.currentTrace = Trace(self.traces)
                # next line will raise the StopIteration
                self.currentTrace.load(nb=1)
                return self.currentTrace
        return TracesIterator(self)


class ProcessedTracesToOrdered(OrderedProcessedTraces):
    def __init__(self, traces):
        assert isinstance(traces, ProcessedTraces)
        self.traces = traces

    def __iter__(self):
        class Iterator:
            def __init__(self, traces):
                self.traces = traces.__iter__()
                self.currentTrace = None

            def __next__(self):
                if self.currentTrace is None:
                    # the final StopIteration is raised from nex line
                    self.currentTrace = next(self.traces).__iter__()
                try:
                    return next(self.currentTrace)
                except StopIteration:
                    self.currentTrace = None
                    return OrderedProcessedTraces.endOfTrace()
        return Iterator(self.traces)


class ProcessedTracesToStream(OrderedProcessedTraces):
    class Iterator:
        def writeLength(self):
            self.stream.seek(self.streamStartPos)
            while self.stream.tell() != self.toErase:
                self.stream.write(' ')
            self.stream.seek(self.streamStartPos)
            self.stream.write(json.dumps(
                {'traces': self.traceNumber, 'events': self.eventNumber}))
            self.toErase = self.stream.tell()

        def __init__(self, parent, stream, callback):
            self.parentIt = parent._traces.__iter__()
            self.stream = stream
            self.callback = callback
            self.traceNumber = None
            self.eventNumber = None
            self.streamStartPos = stream.tell()
            self.toErase = self.streamStartPos
            self.writeLength()
            self.traceNumber = 0
            self.eventNumber = 0
            # we leave space to write again the length
            self.stream.write(" " * 50)
            self.stream.write('\n')

        def __next__(self):
            event = None
            try:
                event = next(self.parentIt)
            except StopIteration:
                self.writeLength()
                self.callback(self.stream)
                raise
            if OrderedProcessedTraces.isEndOfTrace(event):
                self.traceNumber += 1
                self.stream.write('endOfTrace\n')
            else:
                self.eventNumber += 1
                jsonEvent = event.toJson()
                assert '\n' not in jsonEvent
                self.stream.write(jsonEvent + '\n')
            return event

    def __init__(self, traces, streamBuilder, callback):
        self._traces = OrderedProcessedTraces.fromBase(traces)
        self.streamBuilder = streamBuilder
        self.callback = callback

    def __iter__(self):
        stream = self.streamBuilder()
        if stream is None:
            print("no stream provided for writing trace")
            return self.traces.__iter__()
        return __class__.Iterator(self, stream, self.callback)


class StreamToProcessedTraces(OrderedProcessedTraces):
    class Iterator:
        def __init__(self, parent):
            self.parent = parent
            self.stream, self.length = parent.streamBuilder()
            self.readEvents = 0
            self.readTraces = 0
            self.percent = None

        def __next__(self):
            if self.length is not None:
                if self.length['events'] is not None:
                    p = "unknown"
                    if self.length['events'] != 0:
                        p = int(self.readEvents * 100 / self.length['events'])
                    if p != self.percent:
                        print("{}% of cache file is loaded (trace {}/{})".format(p,
                                                                                 self.readTraces + 1, self.length['traces']))
                        self.percent = p
            line = self.stream.readline()
            if line == "":
                self.parent.callback(self.stream)
                raise StopIteration
            elif line == "endOfTrace\n":
                self.readTraces += 1
                return OrderedProcessedTraces.endOfTrace()
            else:
                assert line[-1] == '\n'
                self.readEvents += 1
                return ProcessedEvent.fromJson(line[:-1])

    def __init__(self, streamBuilder, callback):
        self.streamBuilder = streamBuilder
        self.callback = callback

    def __iter__(self):
        return __class__.Iterator(self)


class RamProcessedTraces(ProcessedTraces):
    def __init__(self, source):
        self.traces = []
        if source is None:
            self.generator = None
        else:
            self.generator = ProcessedTraces.fromBase(source).__iter__()

    def _loadNext(self):
        assert self.generator is not None
        try:
            trace = RamProcessedTrace(next(self.generator))
            self.traces.append(trace)
            return trace
        except StopIteration:
            self.generator = None
            return None

    def load(self):
        while self.generator is not None:
            self._loadNext()

    def loadAll(self):
        self.load()
        for trace in self.traces:
            trace.load()

    def __len__(self):
        self.load()
        return len(self.traces)

    def __iter__(self):
        for trace in self.traces:
            yield trace
        if self.generator is not None:
            trace = self._loadNext()
            while self.generator is not None:
                yield trace
                trace = self._loadNext()
        try:
            while self.generator is not None:
                trace = next(self.generator)
                self.traces.append(trace)
                yield trace
        except StopIteration:
            self.generator = None


class TracesLoader():
    cacheDir = "./out/traceCache/"

    @staticmethod
    def operationsToFileName(operations):
        ohash = md5(json.dumps(operations).encode('utf-8')).hexdigest()
        return os.path.join(TracesLoader.cacheDir, ohash)

    def __init__(self, tracesProcessor):
        self.traces = RamProcessedTraces(source=None)
        self.processor = tracesProcessor
        self.appliedOperations = []
        self.pendingOperations = []

        def createAttr(operator):
            setattr(self, operator.name, lambda **
                    args: self.apply(operator.makeOperation(**args)))
        for operator in tracesProcessor.operators.values():
            if not hasattr(self, operator.name):
                createAttr(operator)

    def apply(self, operation):
        """
        Add an operation on the traces. Notice that the computation of the operation will take place later because we wait to have the full list of operations to check if a cache is available.
        :param operation: this object should be build through an operator registered in the TracesProcessor.
        """
        self.pendingOperations.append(operation)

    def makeCache(self, applyPending=True, overwrite=False, imediate=False):
        """save this data in cache for a quicker loading next time"""
        if (applyPending):
            self.applyPending()
        cacheFile = TracesLoader.operationsToFileName(self.appliedOperations)
        cacheDir = os.path.dirname(os.path.abspath(cacheFile))
        if not os.path.exists(cacheDir):
            os.makedirs(cacheDir)
        if os.path.exists(cacheFile) and not overwrite:
            return
        stream = open(cacheFile + ".part", 'w', encoding='utf-8')
        stream.write(json.dumps(self.appliedOperations))
        stream.write('\n')
        stream.write('length:')

        setattr(stream, 'used', False)

        def streamBuilder():
            if stream.used:
                print('cache stream is already used for writing cache')
                return None
            stream.used = True

            print('starting to write file' + cacheFile)
            return stream

        def closeAndMove(callBackStream):
            assert stream is callBackStream
            stream.close()
            os.rename(stream.name, cacheFile)
            print("Cache file sucessfully written")
        self.traces = ProcessedTracesToStream(
            self.traces, streamBuilder, closeAndMove)
        if imediate:
            self.traces = RamProcessedTraces(self.traces)
            self.traces.loadAll()
            assert stream.closed

    def loadFromCache(self, f):
        """
        Load operations and data from a cache file.
        This method is probably useless for a normal user
        """
        stream = open(f, 'r', encoding='utf-8')
        firstLine = stream.readline()
        self.appliedOperations = json.loads(firstLine)
        self.pendingOperations = []
        lengthLine = stream.readline()
        length = None
        if lengthLine.startswith("length:"):
            length = json.loads(lengthLine[7:])

        stream.close()

        def closeFile(stream):
            stream.close()

        def streamBuilder():
            stream = open(f, 'r', encoding='utf-8')
            line = stream.readline()
            if length is not None:
                stream.readline()
            assert line == firstLine, "file was changed between readings"
            return stream, length

        self.traces = StreamToProcessedTraces(
            streamBuilder, callback=closeFile)

    def applyPending(self):
        """
        Apply the operations recorded or load data from the cache.
        Actually, some operations will not compute the new traces but only provide an iterator and the computation will take place when reading the traces
        """

        if len(self.pendingOperations) == 0:
            return
        else:
            # try to get cached traces :
            totalOperations = self.appliedOperations + self.pendingOperations
            for i in range(
                len(totalOperations), len(
                    self.appliedOperations), -1):
                applied = totalOperations[:i]
                pending = totalOperations[i:]
                f = TracesLoader.operationsToFileName(applied)
                if (os.path.exists(f)):
                    print(
                        "Reusing {} operations from cache file {}".format(
                            i, f))
                    self.loadFromCache(f)
                    assert applied == self.appliedOperations
                    self.pendingOperations = pending
                    break
            # apply remaining operations
            while len(self.pendingOperations) > 0:
                self.traces = self.processor.apply(
                    self.pendingOperations[0], self.traces)
                self.appliedOperations.append(self.pendingOperations[0])
                self.pendingOperations = self.pendingOperations[1:]

    def getTraces(self):
        """
        Get traces ready to read.
        you might want to call makeCache() before in order to store the processed data and save computation time.
        It is not advised to store the result of this method, it is better to call it each time you need the trace.
        """
        self.applyPending()
        if not isinstance(self.traces, RamProcessedTraces):
            self.traces = RamProcessedTraces(self.traces)
        return self.traces

    def copy(self):
        """
        get a new TracesLoader with the same operations
        """
        new = ProcessedTraces(self.tracesProcessor)
        new.pendingOperations = self.appliedOperations + self.pendingOperations
        return new


class RetainIterator:
    def __init__(self, iterator):
        self.parentIterator = iterator
        self.nextValue = None
        self.nextLoaded = False

    def __next__(self):
        return self.pop()

    def show(self):
        if not self.nextLoaded:
            self.nextValue = next(self.parentIterator)
            self.nextLoaded = True
        return self.nextValue

    def pop(self):
        if self.nextLoaded:
            e = self.nextValue
            self.nextValue = None
            self.nextLoaded = False
            return e
        else:
            return next(self.parentIterator)


class MultiRetainIterator:
    def __init__(self, iterator):
        self.parentIterator = iterator
        self.loaded = []

    def __next__(self):
        return self.pop()

    def show(self, pos):
        try:
            while pos >= len(self.loaded) and self.parentIterator is not None:
                self.loaded.append(next(self.parentIterator))
        except StopIteration:
            self.parentIterator = None
            raise
        if pos >= len(self.loaded):
            raise StopIteration
        return self.loaded[pos]

    def pop(self):
        if len(self.loaded) > 0:
            return self.loaded.pop(0)
        if self.parentIterator is None:
            raise StopIteration
        return next(self.parentIterator)


class MapTraces(ProcessedTraces):
    class Iterator:
        def __init__(self, parent):
            self.iterator = parent.traces.__iter__()
            self.parent = parent

        def __next__(self):
            trace = next(self.iterator)
            assert isinstance(trace, ProcessedTrace)
            return self.parent.func(trace)

    def __init__(self, func, traces):
        self.traces = ProcessedTraces.fromBase(traces)
        self.func = func

    def __iter__(self):
        return __class__.Iterator(self)


class MapEventsInTraces(OrderedProcessedTraces):
    """
    Utility class to apply a function on each event of traces
    """
    class Iterator:
        def __init__(self, parent):
            self.iterator = parent.traces.__iter__()
            self.parent = parent

        def __next__(self):
            event = next(self.iterator)
            if OrderedProcessedTraces.isEndOfTrace(event):
                return event
            assert isinstance(event, ProcessedEvent)
            event = self.parent.func(event)
            assert isinstance(event, ProcessedEvent)
            return event

    def __init__(self, func, traces):
        self.traces = OrderedProcessedTraces.fromBase(traces)
        self.func = func

    def __iter__(self):
        return __class__.Iterator(self)


class TraceOperator():
    def __init__(self):
        self.name = self.__class__.__name__
        self.name = self.name[0].lower() + self.name[1:]

    def apply(self, traces):
        """
        Apply this operation on the given trace with the given attributes and return a new trace or a generator object.
        """
        raise NotImplementedError

    def makeOperation(self, **attrs):
        return{"name": self.name, "args": attrs}


class TracesProcessor:
    class Troncate(TraceOperator):
        def apply(self, traces, length):
            class TroncTrace(ProcessedTrace):
                def __init__(self, trace, length):
                    self.trace = trace
                    self.length = length

                def __iter__(self):
                    elapsed = 0
                    for event in self.trace:
                        if elapsed == length:
                            break
                        yield event
                        elapsed += 1
            return MapTraces(lambda trace: TroncTrace(trace, length), traces)

    class RemoveDoubles(TraceOperator):
        class UniqTrace(OrderedProcessedTraces):
            def __init__(self, trace):
                self.trace = trace

            def __iter__(self):
                return TracesProcessor.RemoveDoubles.Iterator(self)

        class Iterator:
            def __init__(self, parent):
                self.iterator = parent.trace.__iter__()
                self.previous = None

            def __next__(self):
                try:
                    while True:
                        event = next(self.iterator)
                        if self.previous is None:
                            self.previous = event
                            continue
                        if self.previous.data == event.data:
                            self.previous = ProcessedEvent(
                                self.previous.data, originalEvents=[
                                    self.previous, event])
                        else:
                            r = self.previous
                            self.previous = event
                            return r

                except StopIteration:
                    if self.previous is not None:
                        event = self.previous
                        self.previous = None
                        return event
                    else:
                        raise

        def apply(self, traces):
            return MapTraces(
                func=lambda trace: __class__.UniqTrace(trace), traces=traces)

    class RemoveIncomplete(TraceOperator):
        class Iterator:
            def __init__(self, parent):
                self.iterator = parent.traces.__iter__()
                self.parent = parent

            def __next__(self):
                while True:
                    event = next(self.iterator)
                    if OrderedProcessedTraces.isEndOfTrace(event):
                        return event
                    missing = False
                    for key in self.parent.neededKeys:
                        if key not in event.data.keys():
                            print(
                                "missing key '{} in event {}\n\t(from {})".format(
                                    key, event.data, event.originalEvents))
                            missing = True
                            break
                    if missing:
                        continue
                    return event

        class Traces(OrderedProcessedTraces):
            def __init__(self, traces, neededKeys):
                self.traces = OrderedProcessedTraces.fromBase(traces)
                self.neededKeys = neededKeys

            def __iter__(self):
                return TracesProcessor.RemoveIncomplete.Iterator(self)

        def apply(self, traces, neededKeys):
            return __class__.Traces(traces, neededKeys)

    class CutByTime(TraceOperator):
        class Iterator:
            def __init__(self, parent):
                self.iterator = parent.traces.__iter__()
                self.deltaT = parent.deltaT
                self.prevTimestamp = None
                self.pending = None

            def __next__(self):
                if self.pending is not None:
                    event = self.pending
                    self.pending = None
                    return event
                event = next(self.iterator)
                if OrderedProcessedTraces.isEndOfTrace(event):
                    self.prevTimestamp = None
                    return event
                eventTs = event.getTimestamp()
                if self.prevTimestamp is None:
                    self.prevTimestamp = eventTs
                if eventTs is not None and eventTs > self.prevTimestamp + self.deltaT:
                    self.prevTimestamp = eventTs
                    self.pending = event
                    return OrderedProcessedTraces.endOfTrace()
                if eventTs is not None:
                    self.prevTimestamp = eventTs
                return event

        class Traces(OrderedProcessedTraces):
            def __init__(self, traces, deltaT):
                self.traces = OrderedProcessedTraces.fromBase(traces)
                self.deltaT = deltaT

            def __iter__(self):
                return TracesProcessor.CutByTime.Iterator(self)

        def apply(self, traces, deltaT):
            return __class__.Traces(traces, deltaT)

    class AddEndSymbol(TraceOperator):
        class Trace(ProcessedTrace):
            class Iterator:
                def __init__(self, parent):
                    self.iterator = parent.trace.__iter__()
                    self.parent = parent

                def __next__(self):
                    if self.iterator is not None:
                        try:
                            return next(self.iterator)
                        except StopIteration:
                            self.iterator = None
                            return self.parent.endSymbol()
                    raise StopIteration

            def __init__(self, trace):
                self.trace = trace

            def __iter__(self):
                return __class__.Iterator(self)

            def endSymbol(self):
                return ProcessedEvent("End Of Trace")

        def apply(self, traces):
            return MapTraces(func=__class__.Trace, traces=traces)

    class HardCodedTraces(TraceOperator):
        class Traces(ProcessedTraces):
            def __init__(self, prevTraces, rawTraces):
                self.prevTraces = ProcessedTraces.fromBase(prevTraces)
                self.rawTraces = rawTraces

            def __iter__(self):
                for trace in self.prevTraces:
                    yield trace
                for trace in self.rawTraces:
                    yield trace

        class Trace(ProcessedTrace):
            def __init__(self, events):
                self.events = events

            def __iter__(self):
                return self.events.__iter__()

        def apply(self, traces, rawTraces):
            extendedTraces = []
            for trace in rawTraces:
                extended = __class__.Trace(
                    map(lambda event: ProcessedEvent(event), trace))
                extendedTraces.append(extended)
            return __class__.Traces(traces, extendedTraces)

    def __init__(self):
        self.operators = {}
        self.register(TracesProcessor.Troncate())
        self.register(TracesProcessor.HardCodedTraces())
        self.register(__class__.RemoveDoubles())
        self.register(__class__.RemoveIncomplete())
        self.register(__class__.CutByTime())
        self.register(__class__.AddEndSymbol())

    def register(self, operator):
        assert operator.name not in self.operators
        self.operators[operator.name] = operator

    def apply(self, operation, traces):
        assert isinstance(traces, BaseProcessedTraces)
        operator = self.operators[operation['name']]
        arguments = operation['args']
        print("Applying {}({})".format(operator.name, ", ".join(
            map(lambda e: "{}={}".format(e[0], e[1]), arguments.items()))))
        return operator.apply(traces, **arguments)


PROCESSED_DIR = "./out/"


# def preProcessTest(d):
#    result = {}
#    bench = d['bench']
#    if bench == "Livebox_Dev_253":
#        return None
#    result['bench'] = bench
#
#    epoch = datetime.datetime(1970, 1, 1, 0, 0, 0, tzinfo=tzutc())
#    timestamp = d['timestamp']
#    timestamp = dateutil.parser.parse(timestamp)
#    result['timestamp'] = (timestamp - epoch).total_seconds()
#
#    # result['value']=d['value']
#    if 'target' not in d.keys():
#        print ("missing key")
#        return None
#    result['target'] = d['target']
#    result['status'] = d['status']
#
#    return result
#
#
# def loadJson(orangeDataDir, preProcess=preProcessTest):
#    preprocessed = PROCESSED_DIR + "exec_traces.json"
#    filesDir = orangeDataDir + "/test-traces/"
#    d = []
#    if not os.path.exists(preprocessed):
#        i = -1  # set a positive number to limit the number of files to load
#        for file in os.listdir(filesDir):
#            if i == 0:
#                break
#            i = i - 1
#            print("loading " + file, end='')
#            with open(filesDir + file) as json_data:
#                newD = json.load(json_data)
#                newD = filter(lambda x: x is not None, map(preProcess, newD))
#                prev = len(d)
#                d.extend(newD)
#                print(' : ' + str(len(d) - prev))
#        print("writing data…", end='')
#        with open(preprocessed, 'w') as f:
#            json.dump(d, f)
#        print(' done')
#    else:
#        print("loading data…", end='')
#        with open(preprocessed, 'r') as f:
#            d = json.load(f)
#        print(' done')
#    return d
