# -*- coding:utf8
from __future__ import print_function

from trace_utils import *


class LoadJson(TraceOperator):
    class FilesTraces(OrderedProcessedTraces):
        def __init__(self, files, originalTracesDir, previousTraces):
            self.files = files
            self.originalTracesDir = originalTracesDir
            self.previousTraces = previousTraces

        def __iter__(self):
            class Iterator:
                def __init__(self, parent):
                    self.parent = parent
                    self.previous = parent.previousTraces.__iter__()
                    self.files = parent.files[:]
                    self.currentFile = None
                    self.epoch = datetime.datetime(
                        1970, 1, 1, 0, 0, 0, tzinfo=tzutc())

                def __next__(self):
                    if self.previous is not None:
                        try:
                            return next(self.previous)
                        except StopIteration:
                            self.previous = None
                    if self.currentFile is None:
                        if len(self.files) == 0:
                            raise StopIteration
                        f = self.files.pop(0)
                        with open(os.path.join(self.parent.originalTracesDir, f)) as json_data:
                            print("Loading " + f)
                            self.currentFile = json.load(json_data)
                            self.eventsNb = len(self.currentFile)
                    if len(self.currentFile) == 0:
                        self.currentFile = None
                        print("{} events loaded".format(self.eventsNb))
                        if len(self.files) == 0:
                            return OrderedProcessedTraces.endOfTrace()
                        else:
                            return self.__next__()
                    event = self.currentFile.pop(0)
                    timestamp = event['timestamp']
                    timestamp = dateutil.parser.parse(timestamp)
                    event['timestamp'] = (
                        timestamp - self.epoch).total_seconds()
                    return ProcessedEvent(event, originalEvents=event)
            return Iterator(self)

    def __init__(self, originalTracesDir):
        TraceOperator.__init__(self)
        self.originalTracesDir = originalTracesDir

    def apply(self, traces, files):
        traces = OrderedProcessedTraces.fromBase(traces)
        return __class__.FilesTraces(files, self.originalTracesDir, traces)


class Filter(TraceOperator):
    def filter(self, traces, key, value):
        class FilterTrace(ProcessedTrace):
            def __init__(self, trace, key, value):
                self.trace = trace
                self.key = key
                self.value = value

            def __iter__(self):
                for event in self.trace:
                    if isinstance(event.data, dict) and self.key in event.data:
                        if event.data[self.key] != self.value:
                            continue
                    else:
                        originals = event.getOriginalEvents()
                        allMatch = True
                        for original in originals:
                            if original[self.key] != self.value:
                                allMatch = False
                                break
                        if not allMatch:
                            continue
                    yield event
        return MapTraces(lambda trace: FilterTrace(trace, key, value), traces)


class FilterTarget(Filter):
    def apply(self, traces, targetName):
        return self.filter(traces, 'target', targetName)


class FilterBench(Filter):
    def apply(self, traces, benchName):
        return self.filter(traces, 'bench', benchName)


class LiveboxTraceProcessor(TracesProcessor):

    class CleanData(TraceOperator):
        class Iterator:
            def __init__(self, parent):
                self.iterator = parent.traces.__iter__()
                self.parent = parent

            def __next__(self):
                while True:
                    event = next(self.iterator)
                    if OrderedProcessedTraces.isEndOfTrace(event):
                        return event
                    originals = event.getOriginalEvents()
                    # clean operation should take place at the start of loading
                    # process
                    assert originals is not None and len(originals) == 1
                    original = originals[0]
                    if 'target' not in original:
                        print("missing key target in event ", event)
                        continue
                    if original['bench'] == "Livebox_dev_253":
                        continue
                    if event.data['target'].startswith('http'):
                        event.data['target'] = "NavWeb " + event.data['target']
                    return event

        class Traces(OrderedProcessedTraces):
            def __init__(self, traces):
                self.traces = OrderedProcessedTraces.fromBase(traces)

            def __iter__(self):
                return LiveboxTraceProcessor.CleanData.Iterator(self)

        def apply(self, traces):
            return __class__.Traces(traces)

    class ToString(TraceOperator):
        def apply(self, traces, hideHttp, hideZapping, showStatus):
            def eventToString(event):
                target = event.data['target']
                if hideHttp:
                    if target.startswith("NavWeb-Android http"):
                        target = target[0:14]
                    elif target.startswith("NavWeb http"):
                        target = target[0:6]
                if hideZapping and target.startswith("zapping"):
                    target = target[0:7]
                string = target
                if showStatus:
                    string += " " + event.data['status']
                return ProcessedEvent(string, originalEvents=event)
            return MapEventsInTraces(traces=traces, func=eventToString)

    def __init__(self, originalTracesDir):
        TracesProcessor.__init__(self)
        self.register(LiveboxTraceProcessor.CleanData())
        self.register(LoadJson(originalTracesDir))
        self.register(FilterBench())
        self.register(__class__.ToString())


class LiveboxMonitoringProcessor(TracesProcessor):
    @staticmethod
    def getBootTime(event):
        timestamp = event.data['timestamp']
        if 'uptime' in event.data.keys():
            return timestamp - event.data['uptime']
        if event.originalEvents is None:
            return None
        for orig in event.originalEvents:
            if orig['metric'] == 'uptime':
                return orig['timestamp'] - orig['value']
        return None

    class CleanData(TraceOperator):
        class Iterator:
            def __init__(self, parent):
                self.iterator = parent.traces.__iter__()

            def __next__(self):
                event = next(self.iterator)
                if not OrderedProcessedTraces.isEndOfTrace(event):
                    data = {}
                    for key, value in event.data.items():
                        if key == 'target' or key == 'bench':
                            value = value.lower()
                        data[key] = value
                    if data['target'] == 'livebox' and data['node'] == "monitoring" and data['metric'].startswith(
                            "stats->"):
                        data['metric'] = data['metric'][7:]
                    event.data = data
                return event

        class Traces(OrderedProcessedTraces):
            def __init__(self, traces):
                self.traces = OrderedProcessedTraces.fromBase(traces)

            def __iter__(self):
                return LiveboxMonitoringProcessor.CleanData.Iterator(self)

        def apply(self, traces):
            return __class__.Traces(traces)

    class Aggregate(TraceOperator):
        class Iterator:
            def __init__(self, parent):
                self.iterator = RetainIterator(parent.traces.__iter__())
                self.parent = parent

            def __next__(self):
                if self.iterator is None:
                    raise StopIteration
                originals = []
                if OrderedProcessedTraces.isEndOfTrace(self.iterator.show()):
                    return self.iterator.pop()
                start = self.iterator.show().getTimestamp()
                assert start is not None
                while True:
                    event = None
                    try:
                        event = self.iterator.show()
                    except StopIteration:
                        self.iterator = None
                    if self.iterator is None or OrderedProcessedTraces.isEndOfTrace(
                            event) or event.getTimestamp() >= start + self.parent.groupTime:
                        newData = {'timestamp': start}
                        for event in originals:
                            newData[event.data['metric']] = event.data['value']
                        return ProcessedEvent(
                            newData, originalEvents=originals)
                    else:
                        originals.append(self.iterator.pop())

        class Traces(OrderedProcessedTraces):
            def __init__(self, traces, groupTime):
                self.traces = OrderedProcessedTraces.fromBase(traces)
                self.groupTime = groupTime

            def __iter__(self):
                return LiveboxMonitoringProcessor.Aggregate.Iterator(self)

        def apply(self, traces, groupTime):
            """
            Aggregate events with similar timestamp.
            :param sampling: the interval between each produced event
            """
            return __class__.Traces(traces, groupTime)

    class TagByReboot(TraceOperator):
        class Iterator:
            def __init__(self, parent):
                self.iterator = MultiRetainIterator(parent.traces.__iter__())
                self.parent = parent

            def __next__(self):
                event = self.iterator.pop()
                if OrderedProcessedTraces.isEndOfTrace(event):
                    return event
                eventTs = event.getTimestamp()
                if eventTs is None:
                    print("an event cannot be tagged because of missing timestamp")
                    return event
                pos = 0
                countedEvents = 0
                outOfScope = False
                while True:
                    try:
                        otherEvent = self.iterator.show(pos)
                    except StopIteration:
                        print("end of trace, cannot tag event")
                        return event
                    pos += 1
                    if OrderedProcessedTraces.isEndOfTrace(otherEvent):
                        continue
                    otherEventBoot = LiveboxMonitoringProcessor.getBootTime(
                        otherEvent)
                    if otherEventBoot is None:
                        continue
                    countedEvents += 1
                    if self.parent.preBootTime is not None:
                        otherTs = otherEvent.getTimestamp()
                        if otherTs is not None:
                            if self.parent.preBootTime + eventTs < otherTs:
                                # We have loaded enough events
                                outOfScope = True
                    if self.parent.preBootNumber is not None:
                        if countedEvents > self.parent.preBootNumber:
                            outOfScope = True

                    if outOfScope:
                        event.data[self.parent.labelName] = "PreNonReboot"
                        return event
                    if eventTs < otherEventBoot:
                        event.data[self.parent.labelName] = "PreReboot"
                        return event

        class Traces(OrderedProcessedTraces):
            def __init__(self, traces, preBootTime, preBootNumber, labelName):
                self.preBootTime = preBootTime
                self.preBootNumber = preBootNumber
                self.traces = OrderedProcessedTraces.fromBase(traces)
                self.labelName = labelName

            def __iter__(self):
                return LiveboxMonitoringProcessor.TagByReboot.Iterator(self)

        def apply(self, traces, preBootTime, preBootNumber, labelName):
            return __class__.Traces(
                traces,
                preBootTime=preBootTime,
                preBootNumber=preBootNumber, labelName=labelName)

    class CutByReboot(TraceOperator):
        class Iterator:

            def __init__(self, parent):
                self.iterator = RetainIterator(parent.traces.__iter__())
                self.lastBoot = None

            def __next__(self):
                if OrderedProcessedTraces.isEndOfTrace(self.iterator.show()):
                    self.lastBoot = None
                    return self.iterator.pop()
                ts = LiveboxMonitoringProcessor.getBootTime(
                    self.iterator.show())
                if self.lastBoot is None:
                    self.lastBoot = ts
                    return self.iterator.pop()
                if ts is None:
                    return self.iterator.pop()
                if ts > self.lastBoot + 10:
                    self.lastBoot = None
                    return OrderedProcessedTraces.endOfTrace()
                return self.iterator.pop()

        class Traces(OrderedProcessedTraces):
            def __init__(self, traces):
                self.traces = OrderedProcessedTraces.fromBase(traces)

            def __iter__(self):
                return LiveboxMonitoringProcessor.CutByReboot.Iterator(self)

                # TODO vérifier s'il ne manque pas la dernière trace
        def apply(self, traces):
            return __class__.Traces(traces)

    def __init__(self, originalTracesDir):
        TracesProcessor.__init__(self)
        self.register(LiveboxMonitoringProcessor.Aggregate())
        self.register(LiveboxMonitoringProcessor.CutByReboot())
        self.register(LoadJson(originalTracesDir))
        self.register(FilterBench())
        self.register(FilterTarget())
        self.register(__class__.CleanData())
        self.register(__class__.TagByReboot())
