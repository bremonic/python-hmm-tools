# repository to share and keep traces of work on philae with hidden Markov models


## Experiments

There are two executable scripts: `hmm_livebox.py` and `hmm_scanette.py`

The argument management is poor, the syntax is the following : `--arg value`, a syntax with `=` will not work.

Both of the script accept the  arguments :
+ `--states-number` to set the number of state in some HMM build by those scripts.
+ `--outDir` to indicate where to write the computed models

In addition, the scripts have dedicated arguments:

+ `hmm_livebox.py` :
    + `--orange-data` to indicate where are stored orange traces.
+ `hmm_scanette.py`
    + `--traceFile` to indicate which trace file should be loaded.
    + `--traceDir` to indicate in which directory the traces are stored.


## Use as library

This repository contains some code for traces management in `trace_utils.py`
(and also `livebox_utils.py` and `scanette_utils.py`) but it will probably be
replaced by agilkia.

The code to work with HMM models is in `hmm_utils.py` (it requires the library hmmlearn).


