# -*- coding:utf8
from __future__ import print_function
from hmm_utils import *
from trace_utils import *
from livebox_utils import *
import os

import options

OrangeDataDir = options.StringOption(
    "--orange-data",
    "../../lig-philae/philae-data/orange/")
StatesNumber = options.IntegerOption(options.STATES_NUMBER_ARG, 20)
OutputDir = options.StringOption(options.OUTPUT_DIR_ARG, "out")

options.Options.parse()

if not os.path.exists(OutputDir.getValue()):
    os.makedirs(OutputDir.getValue())

tl = TracesLoader(LiveboxTraceProcessor(os.path.join(
    OrangeDataDir.getValue(), "test-traces")))
# tl.loadJson(files=["2018-07-15.json"])
tl.loadJson(
    files=os.listdir(
        tl.processor.operators['loadJson'].originalTracesDir)[
            0:40])
tl.cleanData()
tl.filterBench(benchName="Livebox4")
tl.makeCache()
tl.toString(hideHttp=False, hideZapping=True, showStatus=False)
tl.cutByTime(deltaT=1800)
# tl.troncate(length=5)
asList = tl.getTraces().asSimpleList()
print(list(map(len, asList)))
print(len(tl.getTraces().asSimpleList()))
#d = tl.getTraces().asSimpleList()[0]
# print('\n'.join(d))

#print("{} events available".format(len(d)))

symbols = set()
for session in tl.getTraces().asSimpleGenerator():
    for symbol in session:
        symbols.add(symbol)
symbols = list(symbols)
KMeansDir = os.path.join(OutputDir.getValue(), "KMeans")
if not os.path.exists(KMeansDir):
    os.mkdir(KMeansDir)
model = hmm_KMeans(tl.getTraces().asSimpleList(), 5,
                   StatesNumber.getValue(), symbols, outputDir=KMeansDir)

model = HMM.createModel(StatesNumber.getValue())
model.train(tl.getTraces().asSimpleList())

g = Graph(model)
g.exportSingle(os.path.join(OutputDir.getValue(), "hmm.dot"))

simplify(model)
g = Graph(model)
g.exportSingle(os.path.join(OutputDir.getValue(), "hmm_simplified.dot"))
