# -*- coding:utf8

from __future__ import print_function
from hmm_utils import *
from scanette_utils import *
import options
from functools import reduce
import agilkia

TraceFile = options.StringOption("--traceFile", "65000-steps.csv")
TraceDir = options.StringOption("--traceDir", "traces_scanette")
StatesNumber = options.IntegerOption(options.STATES_NUMBER_ARG, 10)
OutputDir = options.StringOption(options.OUTPUT_DIR_ARG, "out")

options.Options.parse()

if not os.path.exists(OutputDir.getValue()):
    os.makedirs(OutputDir.getValue())

tl = TracesLoader(ScanetteTraceProcessor(TraceDir.getValue()))
tl.loadTraces(files=[TraceFile.getValue()])
tl.filterScan()
tl.cutByClient()
tl.toAlphabet(
    includeResult=False,
    includeScanParams=False,
    includeObject=False)
# tl.removeDoubles()
tl.makeCache()


# d=tl.getTraces().asSimpleList()
# print("\n".join(list(map(",".join,d))))

sessions = tl.getTraces().asSimpleList()
symbols = set()
for session in sessions:
    for symbol in session:
        symbols.add(symbol)
symbols = list(symbols)


def coverage(traces: ProcessedTraces):
    coverageDir = os.path.join(OutputDir.getValue(), "coverage")
    if not os.path.exists(coverageDir):
        os.mkdir(coverageDir)
    traceset = traces.asAgilkiaTraceSet(eventToAgilkia=scanetteEventToAgilkia)
    sessions = traces.asEventList()

    hmmCluster = HMM_ClusterAlgo(K=5, states=20)
    hmmCluster.fit(traceset)
    hmmCluster.visualize(traceset, simplifyModel=False).exportSingle(os.path.join(coverageDir, "clusters.dot"))

    clustersLabels = hmmCluster.predict(traceset)
    symbolsSeqs = hmmCluster.tracesSetToSymbolSeq(traceset)
    byCluster = []
    for i in range(max(clustersLabels) + 1):
        byCluster.append([])
    for i in range(len(clustersLabels)):
        byCluster[clustersLabels[i]].append(traceset[i])
    internalModel = ScanetteModel('scanette.dot')
    colors = ColorList()
    i = 0
    for l in byCluster:
        plot = internalModel.plotableModel()
        color = colors.pickColor()
        plot.plotSequencesWithColors(
            l,
            color)
        plot.export(os.path.join(coverageDir, "internalScanette_cluster{}.svg".format(i)))
        i += 1
    internalModel = ScanetteModel('scanette.dot')
    plot = internalModel.plotableModel()
    plot.plotSequencesWithColors(
        traceset,
        Color(0, 0, 255))
    plot.export(os.path.join(coverageDir, "internalScanette_totalCoverage.svg"))


def HMM_example():
    model = HMM.createModel(StatesNumber.getValue(), symbols)
    # model.forceGaucheDroite(up_to=5)
    model.train(sessions)
    Graph(model).exportSingle(os.path.join(OutputDir.getValue(), "hmm.dot"))
    model = simplify(model, remove=True)
    g = Graph(model)
    g.showSequences(sessions, color=Color(255, 255, 255, 0))
    g.exportSingle(os.path.join(OutputDir.getValue(), "hmm_simplified.dot"))
    g = Graph(model)
    g.showSequences(sessions, color=Color(255, 255, 255, 0))
    g.emissionThresold = 0.001
    g.pFormat = lambda x: "{:.1f}%".format(x * 100)
    g.exportSingle(os.path.join(OutputDir.getValue(),
                                "hmm_simplified_precise.dot"))
    g = Graph(model)
    g.showSequences(sessions, color=Color(255, 0, 0))
    g.exportSingle(os.path.join(OutputDir.getValue(),
                                "hmm_simplified_with_seq.dot"))


def createManual():
    print("manual model")

    debloquer = 0
    faire_courses = 1
    transmettre = 2
    relecture = 3
    fin_relecture = 4
    terminer = 5
    manualModel = HMM.createModel(terminer + 1, symbols)

    for i in range(manualModel.statesNumber()):
        for j in range(manualModel.statesNumber()):
            manualModel.setTransition(i, j, 0)
        for sym in range(manualModel.symbolsNumber()):
            manualModel.setEmission(i, sym, 0)
        manualModel.setInitial(i, 0)

    manualModel.setInitial(debloquer, 1)

    manualModel.setTransition(debloquer, faire_courses, 1)
    manualModel.setTransition(faire_courses, faire_courses, 0.5)
    manualModel.setTransition(faire_courses, transmettre, 0.5)
    manualModel.setTransition(transmettre, terminer, 0.5)
    manualModel.setTransition(transmettre, relecture, 0.5)
    manualModel.setTransition(relecture, relecture, 0.5)
    manualModel.setTransition(relecture, fin_relecture, 0.3)
    manualModel.setTransition(relecture, terminer, 0.2)
    manualModel.setTransition(fin_relecture, terminer, 1)
    manualModel.setTransition(terminer, terminer, 1)

    manualModel.setEmission(
        debloquer,
        manualModel.getSymbols().index('debloquer'),
        1)
    manualModel.setEmission(
        faire_courses,
        manualModel.getSymbols().index('scanner'),
        0.9)
    manualModel.setEmission(
        faire_courses,
        manualModel.getSymbols().index('supprimer'),
        0.1)
    manualModel.setEmission(
        transmettre,
        manualModel.getSymbols().index('transmission'),
        1)
    manualModel.setEmission(
        relecture,
        manualModel.getSymbols().index('scanner'),
        1)
    manualModel.setEmission(
        fin_relecture,
        manualModel.getSymbols().index('transmission'),
        1)
    manualModel.setEmission(
        terminer, manualModel.getSymbols().index('abandon'), 1)

    g = Graph(manualModel)
    g.showSequences(sessions, color="#ff0000")
    g.exportSingle(os.path.join(OutputDir.getValue(), "manual.dot"))

    print("refitting manual Model")
    manualModel.train(sessions)
    g = Graph(manualModel)
    g.emissionThresold = 0.00001
    g.transitionThresold = 0.001
    g.pFormat = lambda x: "{:.1f}%".format(x * 100)
    g.showSequences(sessions, color="#ff0000")
    g.exportSingle(os.path.join(OutputDir.getValue(), "manual_refitted.dot"))

    print("refitting randomized manual model")
    manualModel.randomize(noise=0.1)
    manualModel.train(sessions)
    g = Graph(manualModel)
    g.emissionThresold = 0.00001
    g.pFormat = lambda x: x
    g.showSequences(sessions, color="#ff0000")
    g.exportSingle(os.path.join(OutputDir.getValue(),
                                "manual_randomized_refitted.dot"))


#createManual()
coverage(tl.getTraces())
HMM_example()
exit(0)


def KMeans_example():
    tl = TracesLoader(ScanetteTraceProcessor(TraceDir.getValue()))
    tl.loadTraces(files=[TraceFile.getValue()])
    tl.makeCache()
    tl.cutByClient()
    tl.toAlphabet(
        includeResult=True,
        includeScanParams=False,
        includeObject=True)
    #tl.makeCache()

    sessions = tl.getTraces().asSimpleList()
    symbols = set()
    for session in sessions:
        for symbol in session:
            symbols.add(symbol)
    symbols = list(symbols)

    KMeansDir = os.path.join(OutputDir.getValue(), "KMeans")
    if not os.path.exists(KMeansDir):
        os.mkdir(KMeansDir)

    modelPath = os.path.join(KMeansDir, "model.json")
    model = None
    if not os.path.exists(modelPath):
        model = hmm_KMeans(sessions, 5, StatesNumber.getValue(),
                           symbols, outputDir=KMeansDir)
        model.save(os.path.join(KMeansDir, "model.json"))
    else:
        model = HMM.load(os.path.join(KMeansDir, "model.json"))
