#-*- coding:utf8

import sys

STATES_NUMBER_ARG = "--states-number"
OUTPUT_DIR_ARG = "--outDir"


class Options:
    @staticmethod
    def parse():
        args = sys.argv[1:]
        for arg in args:
            if arg == "-h" or arg == "--help":
                for option in Options.instances:
                    print(option.help())
                sys.exit(0)
        for option in Options.instances:
            args = option.parse(args)

    instances = []


class Option:
    def __init__(this, argument, default):
        this.argument = argument
        this.value = default
        Options.instances.append(this)

    def getValue(this):
        return this.value

    def parse(this, args):
        return args

    def help(this):
        return this.argument


class StringOption(Option):
    def parse(this, args):
        for i in range(len(args)):
            if args[i] == this.argument and i + 1 < len(args):
                this.value = args[i + 1]
                return args[0:i] + args[i + 2:]
        return args


class IntegerOption(Option):
    def parse(this, args):
        for i in range(len(args)):
            if args[i] == this.argument and i + 1 < len(args):
                this.value = int(args[i + 1])
                return args[0:i] + args[i + 2:]
        return args
