import ghmm
import random
from hmm_utils import *

"""
In this file, we build random sequences and try to learn them with HMM library.
The goal is to experiment with HMM tools
"""


sigma = ghmm.Alphabet(['A', 'B', 'C'])

A = [[0.7, 0.2, 0.1], [0.5, 0.2, 0.3], [0.4, 0.4, 0.2]]
B = [[0.1, 0.9], [0.9, 0.1], [0.3, 0.7]]
pi = [0.5, 0.5, 0]


A, B, pi = build_random(sigma, 4)


m = ghmm.HMMFromMatrices(sigma, ghmm.DiscreteDistribution(sigma), A, B, pi)


# building a training set
training = []
for i in range(10000):
    seq = []
    for j in range(3):
        seq.append('A')
    for j in range(4):
        seq.append('A' if random.random() > 0.5 else 'B')
    for j in range(3):
        seq.append('B')
    # training.append(ghmm.EmissionSequence(sigma,seq))
    training.append(seq)
training = []
for i in range(1):
    seq = []
    if random.random() < 0.9:
        seq.append('A' if random.random() < 0.1 else 'C')
    for l in range(20000):  # int(random.random()*3)+1):
        for j in range(1):
            seq.append('A')
        for j in range(1):
            seq.append('A' if random.random() > 0.6 else 'B')
        for j in range(1):
            seq.append('B')
    seq.append('C')
    training.append(seq)
training = ghmm.SequenceSet(sigma, training)
print(training)
print(m)
m.baumWelch(training)
print(m)

for i in range(15):
    print m.sampleSingle(10)


Graph(m).exportSingle("/tmp/hmm.dot")
